def makeNewCanvas():
  canvas = makeEmptyPicture(400, 400)
  addRectFilled(canvas, 0, 350, 50, 50, black)
  addRectFilled(canvas, 0, 250, 50, 50, black)
  addRectFilled(canvas, 0, 150, 50, 50, black)
  addRectFilled(canvas, 0, 50, 50, 50, black)
  addRectFilled(canvas, 50, 0, 50, 50, black)
  addRectFilled(canvas, 100, 50, 50, 50, black)
  addRectFilled(canvas, 200, 50, 50, 50, black)
  addRectFilled(canvas, 150, 0, 50, 50, black)
  addRectFilled(canvas, 250, 0, 50, 50, black)
  addRectFilled(canvas, 300, 50, 50, 50, black)
  addRectFilled(canvas, 350, 0, 50, 50, black)
  addRectFilled(canvas, 50, 100, 50, 50, black)
  addRectFilled(canvas, 50, 200, 50, 50, black)
  addRectFilled(canvas, 50, 300, 50, 50, black)
  addRectFilled(canvas, 100, 150, 50, 50, black)
  addRectFilled(canvas, 100, 250, 50, 50, black)
  addRectFilled(canvas, 100, 350, 50, 50, black)
  addRectFilled(canvas, 150, 100, 50, 50, black)
  addRectFilled(canvas, 150, 200, 50, 50, black)
  addRectFilled(canvas, 150, 300, 50, 50, black)
  addRectFilled(canvas, 200, 150, 50, 50, black)
  addRectFilled(canvas, 200, 250, 50, 50, black)
  addRectFilled(canvas, 200, 350, 50, 50, black)
  addRectFilled(canvas,250, 100, 50, 50, black)
  addRectFilled(canvas,250, 200, 50, 50, black)
  addRectFilled(canvas,250, 300, 50, 50, black)
  addRectFilled(canvas,300, 150, 50, 50, black)
  addRectFilled(canvas,300, 250, 50, 50, black)
  addRectFilled(canvas,300, 350, 50, 50, black)
  addRectFilled(canvas,350, 100, 50, 50, black)
  addRectFilled(canvas,350, 200, 50, 50, black)
  addRectFilled(canvas,350, 300, 50, 50, black)
  addRectFilled(canvas,350, 400, 50, 50, black)
  show(canvas)

makeNewCanvas()